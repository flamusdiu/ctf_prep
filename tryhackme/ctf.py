import json
import platform
import re
import socket
import ssl
import string
import subprocess
import sys
import time
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from pathlib import Path
from types import SimpleNamespace as Namespace
from typing import Union

import requests
from bs4 import BeautifulSoup
from markdownify import MarkdownConverter
from selenium import webdriver

# Set of domains to allow downloading froms
whitelist_domains = [
    "drive.google.com",
    "tryhackme.com",
    "i.imgur.com",
    "tryhackme-images.s3.amazonaws.com",
    "assets.tryhackme.com",
]

ssl._create_default_https_context = ssl._create_unverified_context
requests.packages.urllib3.disable_warnings()

if platform.system() == "Windows":
    SYS_NULL = ""
    FIREFOX_BINARY = "C:\\Program Files\\Mozilla Firefox\\firefox.exe"
else:
    SYS_NULL = "/dev/null"
    FIREFOX_BINARY = "/usr/bin/firefox"

# REGEX Objects
WHITELISTED_DOMAINS_REGEX = re.compile(rf"{'|'.join(whitelist_domains)}")
HTML_TAG_REGEX = re.compile(r"<(?P<tag>\w+)[^>]+>(?P<tag_text>[^<]+)</(?P=tag)>")
MD_URL_REGEX = re.compile(r"(?m)\[(?P<link_title>[^\]]+)\]\((?P<link>https?:\/\/[^)]+)\)")


def md(soup, **options):
    return MarkdownConverter(**options).convert_soup(soup)


class BrowserProxy:
    def __init__(self):
        self.locale_port()

    def start(self) -> None:
        print(f"\nStarting local proxy on port {self.port}")
        self.proxy = subprocess.Popen(
            [
                "mitmdump",
                "-q",
                "--ssl-insecure",
                "--listen-port",
                str(self.port),
                "--stickycookie",
                ".*",
            ]
        )

        start = time.process_time()
        while True:
            try:
                # check the internal web server to see if working
                resp = requests.get(
                    "http://mitm.it",
                    proxies=BrowserProxy.get_proxy_configuration("requests"),
                    verify=False,
                )
                if resp.status_code == 200:
                    break
            except (  # noqa: F841                 ConnectionRefusedError,
                requests.exceptions.ProxyError,
            ) as ex:
                # Prints counter in seconds
                end = time.process_time()
                print(f"waiting ... {round(end - start)}s", end="\r")
                if round(end - start) >= 30:
                    print("Failed to connect to proxy. Exiting...")
                    self.stop()
                    exit(1)
        print("\n")

    def stop(self) -> None:
        print(f"\nTerminating proxy on port {self.port}")
        self.proxy.terminate()

    def locale_port(self) -> None:
        a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        port = 8080
        while True:
            location = ("127.0.0.1", port)
            result_of_check = a_socket.connect_ex(location)
            if result_of_check == 0:
                port += 1
            else:
                BrowserProxy.port = port
                a_socket.close()
                break

    @staticmethod
    def get_proxy_configuration(type="selenium") -> dict:
        if type == "selenium":
            return {
                "proxyType": "manual",
                "httpProxy": f"localhost:{BrowserProxy.port}",
                "sslProxy": f"localhost:{BrowserProxy.port}",
            }
        elif type == "requests":
            return {
                "http": f"http://127.0.0.1:{BrowserProxy.port}",
                "https": f"http://127.0.0.1:{BrowserProxy.port}",
            }
        else:
            return {}


class FileName:
    ext: str = ""

    def __set_name__(self, owner, name):
        self.name = str(name)

    def __get__(self, obj, type=None) -> str:
        return obj.__dict__.get(self.name) or ""

    def __set__(self, obj, value) -> None:
        if isinstance(value, str):
            delete_dict = {
                sp_character: ""
                for sp_character in string.punctuation
                if sp_character != "."
            }

            delete_dict[" "] = "-"
            table = str.maketrans(delete_dict)
            value = HTML_TAG_REGEX.sub(r"\g<tag_text>", value)
            value = value.translate(table).lower()
            obj.__dict__[self.name] = f"{value}"

        else:
            raise TypeError(
                f"Got {type(value)} instead of {type(str)}!"
                f"Error setting JSON on {str(obj)}"
            )


@dataclass
class URL:

    url: Union[str, tuple]
    name: str = field(init=True, default="")

    def __eq__(self, other) -> bool:
        if isinstance(other, URL):
            return self.url == other.url

    def __hash__(self) -> int:
        return hash(self.__repr__())

    def __str__(self) -> str:
        return self.url


@dataclass
class CTFTaskQuestion:
    task_id: int
    question_id: int
    content: str
    hint: str

    def __post_init__(self):
        self.content = md(BeautifulSoup(self.content, "html.parser"))
        self.hint = self.hint.replace("/", "&frasl;")
        self.hint = self.hint.replace("\\", "&Backslash;")
        self.hint = md(BeautifulSoup(self.hint, "html.parser"))


@dataclass
class CTFTaskFile:
    url: URL
    request: str = field(init=True, default="get")
    filename: str = field(init=False, default=FileName())
    data: dict = field(init=True, default_factory=lambda: {})
    save_location: str = field(init=False, default="")
    downladable: bool = field(init=False, default=False)

    def download(self, location: Union[Path, str]) -> None:
        if type(location) == str:
            location = Path(str)

        location.mkdir(parents=True, exist_ok=True)

        if "drive.google.com" in self.url.url:
            regex = r"d/\b([-a-zA-Z0-9(!@:%_\+.~#?&\/\/=]*)/"
            matches = re.finditer(regex, self.url.path, re.MULTILINE)

            for match in matches:
                file_id = match.group()

            download_url = f"https://drive.google.com/uc?export=download&id={file_id}"
        elif "tryhackme.com" in self.url.url:
            if not self.data:
                return False
            download_url = "https://tryhackme.com/material/deploy"
        else:
            download_url = str(self.url.url)

        if self.request == "post":
            req = requests.post(
                download_url,
                allow_redirects=True,
                data=self.data,
                proxies=BrowserProxy.get_proxy_configuration("requests"),
                verify=False,
            )
        else:
            req = requests.get(
                download_url,
                allow_redirects=True,
            )

        try:
            self.filename = re.sub(
                '"', "", req.headers["content-disposition"].split(";")[1].split("=")[1]
            )
        except KeyError:
            # check for file name at the end of path
            regex = r"\b(?P<file_name>[^/s]+)$"
            self.filename = re.search(regex, self.url.url).group("file_name")

        finally:
            if req.status_code == 200:
                self.save_location = Path(location / self.filename)
                if not self.save_location.exists():
                    self.save_location.write_bytes(req.content)
                    print(f"---> {Path(location / self.filename)} saved.")
                else:
                    print(f"---> {Path(location / self.filename)} exists. Skipped!")
            else:
                print(
                    f"----> Download failed with status {req.status_code} for {download_url}!"
                )
                if self.request == "post":
                    print(f"----> Request data: {self.data!s}")


class Session:
    def __init__(self) -> None:
        self.logged_in_check_url = ""

    def __call__(
        self,
        uri: str,
        proxy: dict,
        logged_in_check_url: str = "",
    ) -> None:
        self.uri = uri
        self.proxy = proxy
        if not self.logged_in_check_url:
            self.logged_in_check_url = logged_in_check_url
        if not self.check_if_logged_in():
            desired_capabilities = webdriver.DesiredCapabilities.FIREFOX.copy()
            desired_capabilities["proxy"] = browserproxy.get_proxy_configuration(
                type="selenium"
            )

            options = webdriver.FirefoxOptions()
            options.headless = False
            options.binary_location = FIREFOX_BINARY
            print("Starting Firefox to login...Close window after login!")

            with webdriver.Firefox(
                options=options,
                desired_capabilities=desired_capabilities,
                service_log_path=SYS_NULL,
            ) as driver:

                uri = f"https://{baseuri}/dashboard"
                print(f"\nOpening page {uri}...")
                driver.get(uri)
                driver.set_window_position(0, 0)
                driver.set_window_size(1200, 1700)

                input("Press Enter to continue...")

    def check_if_logged_in(self):
        resp = requests.get(
            self.logged_in_check_url,
            allow_redirects=False,
            proxies=self.proxy,
            verify=False,
        )

        if resp.status_code == 302:
            print("Not logged in or login failed!")
            return False
        return True


sslSession = Session()


@dataclass
class CTFMarkdownAbstract(ABC):
    title: str
    mdsave_dir: Path
    image_save_dir: Path
    cjson: json = field(repr=True)
    mdFile: str = field(init=False, default="")
    file_name: str = field(init=True, default=FileName())
    questions: list[CTFTaskQuestion] = field(init=True, default_factory=lambda: [])
    urls: set[URL] = field(init=False, default_factory=lambda: {})
    files: list[CTFTaskFile] = field(init=True, default_factory=lambda: [])

    def __post_init__(self):
        self.title = HTML_TAG_REGEX.sub(r"**\g<tag_text>**", self.title)

    def save(self):
        print(
            f"--> Markdown saved for {self.title} @ {self.mdsave_dir / self.file_name}.md"
        )
        md_file = Path(self.mdsave_dir / f"{self.file_name}.md")
        md_file.write_text(self.mdFile, encoding="utf-8")

    @staticmethod
    def get_urls(md_images: list) -> list:

        urls = set()

        for image in md_images:
            src = image.attrs.get("src", "")
            name = image.attrs.get("alt", "")

            if WHITELISTED_DOMAINS_REGEX.search(src):
                oURL = URL(url=src, name=name)
                urls.add(oURL)

        return urls

    @abstractmethod
    def create(self) -> None:
        NotImplementedError("Need to implement create method!")


@dataclass
class CTFMarkdown(CTFMarkdownAbstract):
    def create(self):
        _id = self.cjson["taskNo"]
        title = md(BeautifulSoup(self.cjson["taskTitle"], "html.parser"))
        description_soup = BeautifulSoup(self.cjson["taskDesc"], "html.parser")
        description = md(description_soup)
        mdFile = f"\n# Task#{_id} - {title}\n"

        if len(description) > 0:
            mdFile += f"\n{description}\n"

        if self.cjson.get("taskType", "") == "downloadable":
            mdFile += f"\n## Files\n\n"
            for task_file in self.files:
                mdFile += f"* [{task_file.filename}]({task_file.save_location})"
            mdFile += "\n"

        mdFile += "\n## Questions\n\n"
        for question in self.questions:
            mdFile += f"{question.question_id}. {question.content}"
            if question.hint:
                mdFile += f"\n\n    **Hint:** {question.hint}\n\n"
            else:
                mdFile += "\n\n"

        self.urls = CTFMarkdown.get_urls(description_soup.find_all("img"))
        self.mdFile = mdFile


@dataclass
class CTFTask:
    id: int
    cjson: dict
    cobj: Namespace
    savedir: Path
    tType: str = field(init=False, default="none")
    description: str = field(init=False, default="")
    name: str = field(init=False, default="")
    mdFile: CTFMarkdown = field(init=False)
    questions: list[CTFTaskQuestion] = field(init=False, default_factory=lambda: [])
    files: list[CTFTaskFile] = field(init=False, default_factory=lambda: [])

    def create(self) -> None:
        self._save_json()
        self._questions()
        self._markdown()
        self._save_files()
        self._update_urls()

    def _questions(self) -> None:
        for question in self.cjson["questions"]:
            oQuestion = CTFTaskQuestion(
                task_id=self.id,
                question_id=question["questionNo"],
                content=question["question"],
                hint=question["hint"],
            )
            self.questions.append(oQuestion)

    def _markdown(self) -> None:
        mdFile_save_dir = Path(self.savedir / "solve")
        image_save_dir = Path(self.savedir / "task")
        self.mdFile = CTFMarkdown(
            title=self.name,
            mdsave_dir=mdFile_save_dir,
            image_save_dir=image_save_dir,
            file_name=self.name,
            cjson=self.cjson,
            questions=self.questions,
            files=self.files,
        )
        self.mdFile.create()

    def _save_json(self) -> None:
        obj_s = json.dumps(
            self.cjson, ensure_ascii=False, sort_keys=True, separators=(",", ":")
        )

        self.description = self.cobj.taskDesc
        self.id = self.cobj.taskNo
        self.name = self.cobj.taskTitle
        self.tType = self.cobj.taskType
        outfile = self.savedir / "task" / f"{self.id}.json"
        outfile.write_text(obj_s, encoding="utf-8")
        print("->", outfile, file=sys.stderr)

    def _save_files(self) -> None:
        print(f"--> Saving files for Task #{self.id}...")

        if self.tType == "downloadable":
            download_id = self.cjson.get("uploadId")
            url = "https://tryhackme.com/"
            data = {"roomCode": ctf.room, "id": download_id}
            taskFile = CTFTaskFile(URL(url), request="post", data=data)
            taskFile.download(ctf.savedir / "file")
            self.files.append(taskFile)

        for mdURL in self.mdFile.urls:
            taskFile = CTFTaskFile(mdURL)
            taskFile.download(ctf.savedir / "file")
            self.files.append(taskFile)

    def _update_urls(self) -> None:
        print(f"--> Updating File/Image URLs to use relative links...")
        links_update = []

        for task_file in self.files:
            save_location = str(task_file.save_location).replace("\\", "/")
            links_update.append((str(task_file.url), f"/{save_location}"))

        mdFile = self.mdFile.mdFile
        [mdFile := mdFile.replace(a, b) for a, b in links_update]
        self.mdFile.mdFile = mdFile

    def save(self) -> None:
        if not (self.mdFile.mdsave_dir / Path(self.mdFile.file_name + ".md")).exists():
            self.mdFile.save()
        else:
            print("--> Markdown file already exists for challenge.", file=sys.stderr)


@dataclass
class CTFTableOfContents(CTFMarkdownAbstract):
    tasks: list[CTFTask] = field(init=True, default_factory=lambda: [])
    room_info: dict = field(init=True, default_factory=lambda: {})

    def create(self):
        room_info = self.room_info
        self.file_name = "index"

        class File:
            name = FileName()

        file_name = File()

        mdToc = f"# {room_info['title']}\n\n"
        mdToc += f"{room_info['description']}\n\n"

        if "video" in room_info:
            mdToc += (
                f'<object id="yt-vid" width="100%" height="600px" '
                f'data="{room_info["video"]["url"]}">'
                f"</object><br />"
                f'YT: [{room_info["video"]["title"]}]({room_info["video"]["url"]})\n\n'
            )

        mdToc += "## Tasks\n"

        tasks_list = []

        for task in self.tasks:
            _id = task.id
            name = HTML_TAG_REGEX.sub(
                "**\g<tag_text>**",
                task.cobj.taskTitle,
            )
            file_name.name = task.cobj.taskTitle
            tasks_list.append(f"[T#{_id} {name}]({file_name.name}.md)")
        mdToc += "\n- " + ("\n- ").join(tasks_list)

        self.mdFile = mdToc + "\n"


@dataclass
class CTF:
    baseuri_url: str
    room: str
    page_data: dict = field(init=False)
    baseuri_chall: str = field(init=False)
    baseuri_file: str = field(init=False)
    savedir: Union[str, Path] = field(init=False)
    cjson: dict = field(init=False, repr=False)
    room_info: json = field(init=False)
    tasks: list[object] = field(init=False, default_factory=lambda: [])

    def __post_init__(self) -> None:
        self.baseuri_file = f"https://{self.baseuri_url}"
        self.baseuri_chall = f"https://{self.baseuri_url}/api/tasks/{self.room}"
        self.baseuri_me = f"https://{self.baseuri_url}/profile"
        self.proxy = BrowserProxy.get_proxy_configuration(type="requests")
        self.savedir = Path(self.baseuri_url) / self.room
        self.savedir.mkdir(parents=True, exist_ok=True)

    def session(self, proxy: dict):
        # Start session. Proxy will automatically capture session cookies.
        sslSession(
            uri=f"https://{self.baseuri_url}",
            proxy=proxy,
            logged_in_check_url=self.baseuri_me,
        )

    def get_room_info(self) -> None:
        url = f"https://{self.baseuri_url}/api/room/details?codes={self.room}&loadWriteUps=true&loadCreators=true&loadUser=true"

        resp = requests.get(url, verify=False, allow_redirects=True, proxies=self.proxy)

        if resp.status_code != 200:
            print(
                f"Request for room details failed! Received status code: {resp.status_code}! Exiting."
            )
            exit()

        room_info_json = json.dumps(
            resp.json(), ensure_ascii=False, sort_keys=True, separators=(",", ":")
        )
        outfile = self.savedir / "room.json"
        outfile.write_text(room_info_json, encoding="utf-8")

        self.room_info = resp.json()[ctf.room]

    def get_tasks(self) -> None:
        resp = requests.get(
            self.baseuri_chall, verify=False, allow_redirects=True, proxies=self.proxy
        )
        chatall_json = json.dumps(
            resp.json(), ensure_ascii=False, sort_keys=True, separators=(",", ":")
        )
        outfile = self.savedir / "chall.json"
        outfile.write_text(chatall_json, encoding="utf-8")

        self.cjson = resp.json()

        assert self.cjson is not None

    def create_tasks(self) -> None:

        outdir = self.savedir
        data = self.cjson["data"]

        for dir in ["file", "task", "solve"]:
            basedir_dir = outdir / dir
            basedir_dir.mkdir(parents=True, exist_ok=True)

        for item in data:
            oTask = Namespace(**item)
            task = CTFTask(
                id=oTask.taskNo,
                savedir=outdir,
                cobj=oTask,
                cjson=self.cjson["data"][int(oTask.taskNo) - 1],
            )
            task.create()
            task.save()
            self.tasks.append(task)

    def create_toc(self) -> None:
        mdToc = CTFTableOfContents(
            room_info=self.room_info,
            tasks=self.tasks,
            cjson=self.cjson,
            title="Tasks",
            mdsave_dir=Path(self.baseuri_url) / self.room / "solve",
            image_save_dir=Path(self.baseuri_url) / self.room / "images",
            file_name="index",
        )

        mdToc.create()
        mdToc.save()


if __name__ == "__main__":

    (room,) = sys.argv[1:]

    baseuri = "tryhackme.com"

    browserproxy = BrowserProxy()
    browserproxy.start()

    ctf = CTF(baseuri, room)
    ctf.session(
        proxy=browserproxy.get_proxy_configuration("requests"),
    )
    if sslSession.check_if_logged_in():
        ctf.get_room_info()
        ctf.get_tasks()
        ctf.create_tasks()
        ctf.create_toc()
