
# Task#17 - Forensics - Hard 
Deploy the instance attached to this task.  The credentials (highlighted in bold) have been provided for you to login via your favourite SSH client to:
    
    IP Address: MACHINE\_IP     
    Username: analysis     
    Password:  tryhackme123     
    *The value for these flags can be found in no particular order.*

## Questions 1. What is the IP address of the attacker who was remotely connected?

2. What is the browsing history of the attacker?

3. What is the name of the user account on the system?

4. What is the password hash for this user? Note: that you are not expected to crack this hash.

5. Find the flag contained within the clipboard.

6. Seek the wisdom of the portal.

