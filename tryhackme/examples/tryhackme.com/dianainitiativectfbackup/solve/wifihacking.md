
# Task#5 - wifi-hacking 
## Questions 1. Title: airWHAT-ng<br />Challenge: What is the name of the tool you would use to turn a WiFi adaptor into monitor mode?

2. Title: $tarT0<br />Challenge: Using the tool from the previous question, what command would you use to put wlan0 into monitor mode?

3. Title: wlan0???<br />Challenge: What will wlan0be renamed to - after executing: airmon-ng start wlan0?

4. Title: What's in the air<br />Challenge: What linux command can you use to find out which wireless interfaces are available?

5. Title: $tarT7<br />Challenge: Type the correct command to turn wlan7 into monitor mode.

6. Title: airoWHAT-ng?<br />Challenge: What is the name of the tool you would use to start WiFi scanning?

7. Title: Ba$iC $cAnNinG<br />Challenge: Using the tool from the previous challenge, type a command to start the most basic Wifi scan for access points using a **wlan0mon** adapter.

