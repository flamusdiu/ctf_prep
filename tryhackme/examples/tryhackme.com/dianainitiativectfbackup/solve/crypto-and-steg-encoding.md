
# Task#9 - crypto (and steg), encoding 
## Questions 1. Title: First translation<br />Challenge: What letter is commonly used to refer to 3324575748?

2. Title: Keep your eyes closed!<br />Challenge: Decrypt: <br />⠚⠋⠝⠋⠕⠗⠆⠙⠇⠊⠵⠞⠛⠞⠎⠉⠛⠚⠑⠑⠎⠝⠉⠑⠅⠗⠑⠑⠎⠺⠉⠞⠛⠽⠆⠞⠛⠺⠉⠏
    ⠛⠗⠍⠑⠟⠥⠵⠞⠛⠝⠇⠑⠖⠥⠗⠎⠅⠢⠉⠧⠥⠎⠎⠏⠚⠧⠓⠋⠟⠧⠵⠎⠚⠟⠵⠧⠟⠞⠅⠛
    ⠛⠚⠙⠥⠕⠍⠎⠉⠛⠢⠓⠧⠎⠝⠎⠺⠊⠢⠊⠧⠛⠗⠉⠏⠛⠟⠵⠑⠍⠝⠎⠭⠊⠗⠑⠑⠲⠝⠚⠎
    ⠅⠧⠃⠞⠕⠎⠅⠇⠊⠚⠑⠥⠍⠟⠅⠧⠓⠥⠖⠞⠆⠿⠿⠿ <br />Flag format: Flag{XXX}

    **Hint:** We touch what we can't see.

3. Title: Scrambbled<br />Challenge: Decrypt:<br />>++++++++++
    [>+++++++>++++++++>+++++++>+++++++>++++++++++++>
    +++++++>+++++++++++>+++++>+++>+++++++++++>++++++
    ++++>+++++++>+++++++++++>+++++>+++++>+++++++++++
    +>+++++++>+++>+++++++++++++><<<<<<<<<<<<<<<<<<<<
    -]>>---->----->+>+++>---->++++>++>+++>>----->>++++>+>+>++
    >->+++>-----><<<<<<<<<<<<<<<<<<<<>.>.>.>.>.>.>.>.>.>.>.>.>
    .>.>.>.>.>.>.
    Flag format: FLAG{XXX}

4. Title: Message intercepted<br />Challenge: You have been hired to learn about a secret meet between Feather and Beaks. After weeks of tailing them, you have intercepted and decoded the following message.<br /><br />Encrypted text:<br />qpte urkh xwj hzfrmr rolmj yzdmg<br /><br />Plain text:<br />Lets meet for coffee later today<br /><br />Using this information, can you find out what their next communication is about?
    
    Decrypt:<br />xfmymezwem ax fpav hy w zwhj jog iek fwivd qod buog zmsy 
    **Hint:** Same lock and key.

5. Title: What is that??<br />Challenge: I am not really sure, are you?<br />0x460x6c0x610x670x7b0x540x680x210x5f0x310x530x5f0x480x650x580x210x7d 
    Flag format: Flag{XXX}

6. Title: The key is in the details.<br />Challenge: Once upon a time Microsoft Paint was an exciting place. It was great for hiding things in plain sight ([Shapeless.jpg](/tryhackme.com/dianainitiativectfbackup/file/shapeless.jpg)). <br />Flag format: flag{XXX}

7. Title: Just keep going.<br />Challenge: One, two, three. How many flags can there actually be in [ColorsandColors.jpg](/tryhackme.com/dianainitiativectfbackup/file/colorsandcolors.jpg)?<br />Flag format: flag{XXX}

8. Title: Peek Inside<br />Challenge: To solve this challenge, you need to crack the encryption of [peek\_inside.zip](/tryhackme.com/dianainitiativectfbackup/file/peekinside.zip).

    **Hint:** Try pkcrack.

9. Title: Visually sound<br />Challenge: There's a message in the waves that unlocks the keys to the castle in [seethesound1.wav](/tryhackme.com/dianainitiativectfbackup/file/seethesound1.wav).

10. Title: Hidden Figures 1<br />Challenge: Who is the camera maker in [index.jpg](/tryhackme.com/dianainitiativectfbackup/file/index.jpg)?

    **Hint:** Properties can show the way.

11. Title: Hidden Figures 2<br />Challenge: What is the camera model in [index.jpg](/tryhackme.com/dianainitiativectfbackup/file/index.jpg)?

    **Hint:** Properties can show the way.

12. Title: Hidden Figures 3<br />Challenge: What is the date that the picture was taken in [index.jpg](/tryhackme.com/dianainitiativectfbackup/file/index.jpg) (mm/dd/yyyy)?

    **Hint:** Properties can show the way.

13. Title: Hidden Figures 4 <br />Challenge: Who is the copyright in [index.jpg](/tryhackme.com/dianainitiativectfbackup/file/index.jpg)?

    **Hint:** Properties can show the way.

14. Title: Hidden Figures 5<br />Challenge: What is the image title in [index.jpg](/tryhackme.com/dianainitiativectfbackup/file/index.jpg)?

    **Hint:** Properties can show the way.

15. Title: Hidden Figures 6<br />Challenge: Who is the creator of the image [index.jpg](/tryhackme.com/dianainitiativectfbackup/file/index.jpg)?

    **Hint:** Designer of TDI 2021 logo.

16. Title: Hidden Figures 7<br />Challenge: What is a tag or keyword on the file [index.jpg](/tryhackme.com/dianainitiativectfbackup/file/index.jpg)?

17. Title: Hidden Figures 8<br />Challenge: What is the subject in [index.jpg](/tryhackme.com/dianainitiativectfbackup/file/index.jpg)?

18. Title: Hidden Figures 9<br />Challenge: If you can look deep inside, the camera model is key. What is the hidden flag in [index.jpg](/tryhackme.com/dianainitiativectfbackup/file/index.jpg)?

    **Hint:** Something's hiding in the image.

19. Title: 2DES MITM<br />Challenge: We captured an encrypted PNG file which we believe has information on a plant that our enemies are targeting. We know they use 2DES for encryption. Using the file [plant.png.2des](/tryhackme.com/dianainitiativectfbackup/file/plant.png.2des), can you figure out what plant is in the picture?
    
    Note: Effective Key Size is 2 bytes<br />Note: Use ECB mod 
    **Hint:** 2DES Meet-in-the-middle 
