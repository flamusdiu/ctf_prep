
# Task#10 - log analysis 
## Questions 1. Title: What's that doing on the Internet?<br />Challenge: After doing a little Googling, we found an IRC log from July 2020 related to alpinelinux.org that contains a SHA512 password. What is the clear text version of that password?

    **Hint:** Dorks are Us.

2. Title: Passwords, passwords, get your clear text passwords!<br />Challenge: There is another password in a log on the Internet, this time it is a root password in cleartext. What is this password found in a March 2020 log related to alpinelinux.org?

    **Hint:** Dorks are Us and we use IRC to prove it.

