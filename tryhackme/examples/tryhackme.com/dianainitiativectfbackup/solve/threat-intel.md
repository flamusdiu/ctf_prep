
# Task#12 - threat intel 
## Questions 1. Title: Protect the peanut butter<br />Challenge: This grocery store has headquarters here: 1500 Brooks Avenue, PO Box 30844, Rochester, NY What was the cause of this grocery store breach in June 2021 (two words)?

2. Title: Man am I stuffed!<br />Challenge: What kind of attack had the grocery store experienced 3 months earlier (two words)?

3. Title: Oopsy - our bad.<br />Challenge: What was in the "re:" line in the breach notice that the grocery store sent out to its customers?

4. Title: Can you hear me now?<br />Challenge: According to the 2021 Verizon Data Breach Investigation Report, what type of data is compromised the quickest?

