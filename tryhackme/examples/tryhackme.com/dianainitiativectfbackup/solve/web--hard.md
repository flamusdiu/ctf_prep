
# Task#16 - Web - Hard 
Start the machine and try and escalate your privileges from a visitor to a blog user to a server manager admin.
    
    Once the machine has started, go to [HTTP://MACHINE\_IP](http://machine_ip/) to begin.

## Questions 1. What is the Blogger user flag?

2. What is the Server Manager admin flag?

