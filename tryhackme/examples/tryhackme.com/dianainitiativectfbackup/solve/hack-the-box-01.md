
# Task#6 - Hack The Box 01 
## Questions 1. Title: $TWTR<br />Challenge: If you know the Twitter handle of the conference organizer then you know the password to the tdi account, you will need that password and a service that runs on port 3389 to answer the rest of the questions. What is the password to the tdi account?

    **Hint:** Twitter ID is 711622477131042816 
2. Title: There's no place like ::1 <br />Challenge: What is the answer to Question 1 in the .txt file located on the desktop of the tdi account? (only the first three parts are needed)

    **Hint:** Use the host command; only the first three parts (quartets) are needed 
3. Title: Swiss Army Knife<br />Challenge: What is the version of the malicious files/zip folder located in the C:\Users\tdi\downloads directory?

    **Hint:** netcat 
4. Title: Lateral Escalation<br />Challenge: What are the contents of the secret.txt file located at C:\Users\student1 
    **Hint:** MS Edge 
5. Title: Are you up to date?<br />Challenge: On what day did this system receive its first Quality Update (m/dd/yyyy)?

    **Hint:** Update History (KB4489899)

6. Title: Voila Trust<br />Challenge: According to the Microsoft Edge (Explorer) browser history, what was the first site visited and/or searched for by the tdi user? Top-level domain only.

    **Hint:** Find the 3 dots! Use only the top level domain.

