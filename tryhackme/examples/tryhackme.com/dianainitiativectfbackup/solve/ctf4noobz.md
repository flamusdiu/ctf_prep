
# Task#13 - CTF4Noobz 
## Questions 1. Title: Honest is Best Policy<br />Challenge: This aspect of the CIA Triad is about ensuring that information is not altered accidentally or by entities unauthorized to make alterations.

2. Title: Plain2Cipher<br />Challenge: What is the term for converting plaintext to ciphertext?

3. Title: Helping you live a healthier, better life through food<br />Challenge: What east coast grocery store suffered a data breach in June 2021?

4. Title: Lots 'o stuff<br />Challenge: This term is used to describe the proliferation of small, connected devices found in many homes and businesses.<br />Credit: NCCAW 2020 Oversee & Govern 
    **Hint:** It's just three letters.

5. Title: MD5(dianainitiative)<br />Challenge: What is the MD5 hash of "dianainitiative"?

    **Hint:** <https://www.md5hashgenerator.com>

6. Title: SHA1(dianainitiative)<br />Challenge: What is the SHA1 hash of "dianainitiative"?

7. Title: Hash type? (1)<br />Challenge: What type of hash is this?<br />e52cac67419a9a224a3b108f3fa6cb6d 
    **Hint:** hash-identifier 
8. Title: Hash type? (2)<br />Challenge: What type of hash is this?<br />7904fc8828c515f5b3f9172bffa197ff 
    **Hint:** hash-identifier 
9. Title: Hash type? 3)<br />Challenge: What type of hash is this?<br />03E4079B565AB2A47A2EFF7F42AE45B8 
    **Hint:** hash-identifier 
10. Title: Password Cracking 1<br />Challenge: What is the password for this hash?<br />7904fc8828c515f5b3f9172bffa197ff 
    **Hint:** crackstation 
11. Title: Password Cracking 2<br />Challenge: What is the password for the NTLM hash?<br />32ed87bdb5fdc5e9cba88547376818d4 
    **Hint:** crackstation 
12. Title: Password Cracking 3<br />Challenge: What is the password for this hash?<br />03E4079B565AB2A47A2EFF7F42AE45B8 
    **Hint:** crackstation 
13. Title: Some Salt 1<br />Challenge: What is the password for the following SHA1 hash?<br />dc6f0dbebfc5747330deeedfbd8475568a740d0a:80808080 
    **Hint:** You can use hashcat and rockyou-75.txt.

14. Title: Some Salt 2<br />Challenge: What is the password for the following SHA-512 hash?<br />FF8D646AC52B7794ADADDAAD606042FF6D2D71C5B91CBF1C1     1D411C790419CF1651EBE71551CD1973ABAC9D32D1392122CC     676F4AA8494E7DA6325A1050FD2DA:3141592653589793238462     6433832795028841     
    Note: The text above includes a small salt, make sure to copy the whole line and not only the first part of the line.<br /><br />

    **Hint:** You can use hashcat and rockyou-75.txt 
15. Title: Bitcoin Hashing Algorithm<br />Challenge: What hashing algorithm does Bitcoin use to hash blocks?

    **Hint:** No dashes.

16. Title: Coffee Break<br />Challenge: We're investigating a potential leak involving a personal Android phone and one employee Tom has been identified as a potential suspect. Tom said that he was on vacation during the time of the incident and he took this photo the day he came back and he said that this photo of his coffee cup can help prove his innocence as he is an avid drinker. Using [coffee.jpg](/tryhackme.com/dianainitiativectfbackup/file/coffee.jpg), what brand of phone does Tom have?
    
    Note: Make\_space\_Model (Case Sensitive)

    **Hint:** <http://fotoforensics.com>

17. Title: Hidden Files 1<br />Challenge: Find the hidden treasure on this website: <https://hidden-files.cityinthe.cloud> .. What is the first flag on the system?
    
    Note: You are allowed to run automated tools on this target. Your scope is limited to the specified hostname & HTTP(S) ports.

    **Hint:** Dirbuster 
18. Title: Hidden Files 2<br />Challenge: Find the hidden treasure on this website: [https://hidden-files.cityinthe.cloud](https://hidden-files.cityinthe.cloud/) .. What is the second flag on the system?
    
    Note: You are allowed to run automated tools on this target. Your scope is limited to the specified hostname & HTTP(S) ports.

    **Hint:** Dirbuster 
19. Title: Hidden Files 3<br />Challenge: Find the hidden treasure on this website: [https://hidden-files.cityinthe.cloud](https://hidden-files.cityinthe.cloud/) .. What is the third flag on the system?
    
    Note: You are allowed to run automated tools on this target. Your scope is limited to the specified hostname & HTTP(S) ports.

    **Hint:** Dirbuster 
20. Title: iot-new 1<br />Challenge: Using [iot-new.pcap](/tryhackme.com/dianainitiativectfbackup/file/iotnew.pcap), what type of device is it?

21. Title: iot-new 2<br />Challenge: Using [iot-new.pcap](/tryhackme.com/dianainitiativectfbackup/file/iotnew.pcap), who is the manufacturer?

22. Title: iot-new 1<br />Challenge: Using [iot-new.pcap](/tryhackme.com/dianainitiativectfbackup/file/iotnew.pcap), what is the model?

23. Title: iot-new 4<br />Challenge: Using [iot-new.pcap](/tryhackme.com/dianainitiativectfbackup/file/iotnew.pcap), What is th device's credentials?

    **Hint:** base64 
