
# Task#11 - forensics 
## Questions 1. Title: http flag flying<br />Challenge: Find the right password and you've found the right flag in [http\_flag.pcap](/tryhackme.com/dianainitiativectfbackup/file/httpflag.pcap).<br />Format: flag{XXX}

2. Title: The Uni-verse is on your side<br />Challenge: Find the hidden flag (note: when/if using Wireshark, an error message of "the capture file appears to be damaged or corrupt" is expected).[find\_the\_flags.pcap](/tryhackme.com/dianainitiativectfbackup/file/findtheflags.pcap).<br />Format: flag{XXX}

    **Hint:** Look at the pcap in a different way. (not inside a pcap tool)

3. Title: Golang RAT<br />Challenge: What protocol is being used for C2 communications in this packet capture [golang\_RAT.pcap](/tryhackme.com/dianainitiativectfbackup/file/golangrat.pcap)?

4. Title: Golang RAT<br />Challenge: What is the C2 domain in the capture [golang\_RAT.pcap](/tryhackme.com/dianainitiativectfbackup/file/golangrat.pcap)? <br /><br />**Caution! This is a malicious domain, do not visit.**

5. Title: Golang RAT<br />Challenge: How many C2 requests were sent in the capture [golang\_RAT.pcap](/tryhackme.com/dianainitiativectfbackup/file/golangrat.pcap)?

6. Title: Golang RAT<br />Challenge: What is the hex value of the protocol flags for the C2 server response in the capture [golang\_RAT.pcap](/tryhackme.com/dianainitiativectfbackup/file/golangrat.pcap)?

7. Title: Golang RAT<br />Challenge: What is the IP address of the victim box in the capture [golang\_RAT.pcap](/tryhackme.com/dianainitiativectfbackup/file/golangrat.pcap)?

