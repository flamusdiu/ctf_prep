
# Task#1 - trivia aka google-fu 
## Questions 1. Title: C in CIA<br />Challenge: The "C" in the "CIA" Triad is for?

2. Title: I in CIA<br />Challenge: The "I" in the "CIA" Triad is for?

3. Title: A in CIA<br />Challenge: The "A" in the "CIA" Triad is for?

4. Title: PNG HEX Signature<br />Challenge: What is the HEX signature for PNG files (answer should not contain spaces)?

5. Title: HackerLadiez 1<br />Challenge: What is the first name of the hacker from Criminal Minds?

    **Hint:** Google 
6. Title: HackerLadiez 2<br />Challenge: What is the first name of the hacker popularized in the Stig Larrson novels?

    **Hint:** Google 
7. Title: HackerLadiez 5<br />Challenge: Who was the founder of the Women's Society of Cyberjutsu? (first last)

    **Hint:** Google 
8. Title: Is this a mistake?<br />Challenge: What UUID is used across all websocket implementations?

    **Hint:** Next in Oprah's reading list is a cute story about a socket who could. RFC 6455.

9. Title: I'm declaring No Shenanigans!<br />Challenge: Which tech company takes shenanigans seriously?

    **Hint:** American cloud communications platform.

10. Title: HackerLadiez 3<br />Challenge: What is the (hacker) name of the character shown in the attached image, [hackerladiez3.jpg](/tryhackme.com/dianainitiativectfbackup/file/hackerladiez3.jpg)?

