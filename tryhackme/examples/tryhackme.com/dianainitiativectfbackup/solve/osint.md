
# Task#2 - osint 
## Questions 1. Title: HackerLadiez 4<br />Challenge: On December 6, 2014, what was the MD5 hash of the logo that appeared on the Women's Society of Cyberjutsu website?

    **Hint:** Wayback machine, hashing tool.

2. Title: Following Crumbs<br />Challenge: Who were the two CFP chairs at the first Diana Initiative Conference? Twitterhandle1 and Twitterhandle2 
    **Hint:** Use archive.org to visit Diana Initiative. Check out CFP.

3. Title: Following Crumbs 1a<br />Challenge: What month year did the Twitterhandle1 join Twitter? (spell out the month yyyy)

    **Hint:** Visit their twitter.

4. Title: Following Crumbs 1b<br />Challenge: On May 21, 2021, Twitterhandle1 called out a certain Senator. What was the senator's Twitter handle?

    **Hint:** Search Twitter from to what.

5. Title: Following Crumbs 1c<br />Challenge: Twitterhandle1 spoke at Bace Cybersecurity Institut in April 2021 about what topic (first word of the title)?

    **Hint:** Retweet 
6. Title: Following Crumbs 1d<br />Challenge: Twitterhandle1 has one of the coveted Vegas-related awards.

    **Hint:** Bio 
7. Title: Following Crumbs 1e<br />Challenge: Twitterhandle1 was a member of which branch of the Military?

    **Hint:** Bio 
8. Title: Following Crumbs 2a<br />Challenge: What month of the year did Twitterhandle2 join Twitter (spell out month yyyy)?

9. Title: Following Crumbs 2b<br />Challenge: What was the full date of Twitterhandles2's first tweet (ddmmyyyy)?

10. Title: Following Crumbs 2c<br />Challenge: What account replied to that first tweet?

    **Hint:** See replies.

11. Title: Following Crumbs 2d<br />Challenge: In what city is the owner of that account located?

    **Hint:** Twitter Bio.

12. Title: Following Crumbs 2e<br />Challenge: That account lists a website in their bio, what is their tagline/slogan (copy verbatim)?

    **Hint:** What do they tell you about them.

13. Title: Following Crumbs 2f<br />Challenge: What is the website listed in Twitterhandle2's bio?

14. Title: Following Crumbs 2g<br />Challenge: Speaking about Twitterhandle2's 411, at which conference did they give their first talk?

    **Hint:** What do they tell you about them?

15. Title: Following Crumbs 2h<br />Challenge: What was the title of the talk?

    **Hint:** What do they tell you about them?

16. Title: Cat in a Bucket?<br />Challenge: The Internet loves cats. Find a gif of a confused kitten with a post-it note on its head. What is the file size in MB?<br />Format: #.##MB 
    **Hint:** Put on a Grayhat to search through AWS buckets.

17. Title: There's no place like ::1<br />Challenge: What was the hashtag used the first time @TyWilson21 tweeted the students?

18. Title: Content What?<br />Challenge: Who is The OSINTIon's Content Distribution Network Provider?

    **Hint:** Just the provider, not the hostnames.

19. Title: Imaginary Clouds<br />Challenge: Which cloud provider's SPECIFIC domain can send email on behalf of NSA?

    **Hint:** You may need some SPF for the burn.

20. Title: Social Messes<br />Challenge: What kind of laptop did Jamey at Walmart use circa February 2019?

    **Hint:** Jamey is a real stand-up guy, but is hiding but if you know his work address...

21. Title: I'll 'rassle ya for it!<br />Challenge: What is the phone number for the mailroom in the building that houses WWE's Global HQ?

22. Title: Delivering the Championship Gold<br />Challenge: Who is WWE's shipping provider (aka who delivers packages to WWE)?

23. Title: World Leaders or World Domination?<br />Challenge: Which former world leader was a Senior Advisor for JP Morgan?

24. Title: It's all about the 000s, 111s, and Coins<br />Challenge: Who owns the BTC address of 34xp4vRoCGJym3xR7yCVPFHoCNxv4Twseo?

25. Title: Ahoy Meow-ty!<br />Challenge: Which non-US Naval Vessel was recently mentioned in a tweet or retweet by Marcelle Lee?

26. Title: Pivot Some More!<br />Challenge: what port is Caddy running on for the HADES host?

27. Title: 8OsR0ck<br />Challenge: A hacker compromised an important server at a company called 8ES\_Rock. They were after a high-value coupon code. Using the [access.log](/tryhackme.com/dianainitiativectfbackup/file/access.log), can you determine what that coupon code was?

    **Hint:** Some encoding schemes can be identified by their ending characters. Do you happen to see anything in the logs that might suggest what scheme was used?

28. Title: 8OsR0ck<br />Challenge: The hacker who compromised the 8ES\_Rock server left an important note on the compromised host. What did they instruct 8ES\_Rock to do?

    **Hint:** This challenge requires you to use clues found in 8ES\_Rock Challenge 1. HINT #1: The note can be found in an artifact dropped by the malware. The hacker may have used a free public sandbox to detonate their warez. HINT #2: Use the proxy logs in Challenge 1 to find the command and control infrastructure where the hash file is stored.

29. Title: 8OsR0ck<br />Challenge: Seems like 8ES\_Rock attacker wasn't done after the first breach. More targets have been identified and attackd. Can you determine the next target on their radar?

    **Hint:** This challenge requires you to use clues from the previous 8ES\_Rock challenges. That said, the hacker has pretty lame OPSEC. The list of targets is available. Its hidden but accessible if you know where to look. And the encoding can be solved if you Follow the right lead.

