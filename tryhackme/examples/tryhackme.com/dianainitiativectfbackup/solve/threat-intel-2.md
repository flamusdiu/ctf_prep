
# Task#14 - Threat Intel 2 
You are working as a cyber threat analyst at Corporate Corporation Inc. The Security Operation Center (SOC) has isolated and sent you some logs.
    
    The logs you received are access.log and auth.log. The attacker IP is 10.0.2.8; it is a local IP taken from the firewall proxy. The external IP used by the attacker is already blocked and can no longer access Corporate Corporation Inc. assets.
    
    They need your help answering important questions related to this threat actor.

## Questions 1. What domain name is potentially linked to the attacker?

2. What username can you find for the attacker?

3. What is the SHA-256 hash of the malware connected to the attacker?

4. What Command and Control infrastructure the attacker is most likely using?

5. Considering the data at hand, what JS library could potentially have known vulnerabilities?

6. Based on his/her social media profile which country is the attacker most likely from?

7. Can you find the string he/she used to advertise they are open to new business opportunities? 

