
# Task#19 - Web - Jarring [Hard]

Start the machine, and then go to [HTTP://MACHINE\_IP](http://machine_ip/):5000 to begin. Challenge may take a few minutes after machine startup for the webpage to appear.

## Questions 1. Carmen Sandiego is at it again with another jarring crime!

    **Hint:** Are you listening for a response?

