
# Task#7 - Web - Graphster [easy/moderate]

<http://52.7.52.108:3000/><br />Note: no scanning or brute-forcing allowed (or needed)

## Questions 1. I made this new song-sharing service for all of my favorite songs, try it out! (1st flag)

    **Hint:** Where is the song data coming from?

2. Have you tried downloading my favorite song? (2nd flag)

    **Hint:** What are the available queries?

3. What are your thoughts on the song? (3rd flag)

    **Hint:** Exif 
