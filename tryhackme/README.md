# CTF Preparations 
## Introduction 
The `ctf.py` script configures and setups a directory for each room on [TryHackMe](https://tryhackme.com). When running this script, it will open a FireFox window to log into TryHackMe. If you have trouble refreshing the screen, try refreshing the page and attempting a new session.

## Example 
I have placed an example inside of the [Examples](examples/) folder.

## Usage 
```bash python ctf.py [roomcode]
```

Example: <https://tryhackme.com/room/dianainitiativectfbackup>

`dianainitiativectfbackup` is the **[roomcode]**. All THM rooms have room codes.

## Requirements 
Requires Python 3.10 or greater.

### Downloads 
- Mozilla's Geckodriver used with Seleinum   - geckodriver - <https://github.com/mozilla/geckodriver/releases>
- Proxy used with Seleinum for SSL connection   - Seleinum does not allow modification of headers   - mitmproxy - <https://mitmproxy.org/>
- Selenium - <https://www.selenium.dev/downloads/>
  - If you do not want to use the python library, download the latest from their site.

### Python 
- requests = "^2.26.0"
- mitmproxy = "^7.0.2"
- selenium = "^3.141.0"
- markdownify = "^0.11.6"
- beautifulsoup4 = "^4.11.1"

### Installation - Poetry 
Install `poetry` following the steps locationed at [Poetry's Doc: Installation](https://python-poetry.org/docs/#installation)

#### Clone this repo 
Clone the repo through git 
```zsh $ git clone https://gitlab.com/flamusdiu/ctf_prep.git ```

#### Virtual Enivonment and Python Requirements 
Create a virtual enviroment and activate it. Poetry will install dependencies at the same time.

```zsh # Ensure you are in the project folder # Installs dependencies and locates python $ poetry install The currently activated Python version 3.8.10 is not supported by the project (^3.9).
Trying to find and use a compatible version. 
Using python3.9 (3.9.5)
Creating virtualenv ctfs-wdxuLL4_-py3.9 in /home/ubuntu/.cache/pypoetry/virtualenvs Installing dependencies from lock file 
Package operations: 55 installs, 0 updates, 0 removals 
  • Installing pycparser (2.20)
  • Installing cffi (1.14.6)
  • Installing markupsafe (2.0.1)
  • Installing click (8.0.1)
  • Installing cryptography (3.4.7)
  • Installing h11 (0.12.0)
  • Installing hpack (4.0.0)
  • Installing hyperframe (6.0.1)
  • Installing itsdangerous (2.0.1)
  • Installing jinja2 (3.0.1)
  • Installing pyasn1 (0.4.8)
  • Installing ruamel.yaml.clib (0.2.6)
  • Installing six (1.16.0)
  • Installing soupsieve (2.2.1)
  • Installing werkzeug (2.0.1)
  • Installing appdirs (1.4.4)
  • Installing asgiref (3.4.1)
  • Installing beautifulsoup4 (4.9.3)
  • Installing blinker (1.4)
  • Installing brotli (1.0.9)
  • Installing certifi (2021.5.30)
  • Installing charset-normalizer (2.0.4)
  • Installing flask (2.0.1)
  • Installing h2 (4.0.0)
  • Installing idna (3.2)
  • Installing kaitaistruct (0.9)
  • Installing ldap3 (2.9.1)
  • Installing mccabe (0.6.1)
  • Installing msgpack (1.0.2)
  • Installing mypy-extensions (0.4.3)
  • Installing passlib (1.7.4)
  • Installing pathspec (0.9.0)
  • Installing protobuf (3.17.3)
  • Installing publicsuffix2 (2.20191221)
  • Installing pycodestyle (2.7.0)
  • Installing pyflakes (2.3.1)
  • Installing pyopenssl (20.0.1)
  • Installing pyparsing (2.4.7)
  • Installing pyperclip (1.8.2)
  • Installing regex (2021.8.3)
  • Installing ruamel.yaml (0.17.10)
  • Installing sortedcontainers (2.4.0)
  • Installing tomli (1.2.1)
  • Installing tornado (6.1)
  • Installing urllib3 (1.26.6)
  • Installing urwid (2.1.2)
  • Installing wsproto (1.0.0)
  • Installing zstandard (0.15.2)
  • Installing black (21.7b0)
  • Installing bs4 (0.0.1)
  • Installing flake8 (3.9.2)
  • Installing markdownify (0.9.2)
  • Installing mitmproxy (7.0.2)
  • Installing requests (2.26.0)
  • Installing selenium (3.141.0)

# Moves into virtual environment $ poetry shell The currently activated Python version 3.8.10 is not supported by the project (^3.9).
Trying to find and use a compatible version. 
Using python3.9 (3.9.5)
Spawning shell within /home/<user>/.cache/pypoetry/virtualenvs/ctfs-wdxuLL4_-py3.9 $ . /home/<user>/.cache/pypoetry/virtualenvs/ctfs-wdxuLL4_-py3.9/bin/activate (ctfs-wdxuLL4_-py3.9) $ 
```

#### Download 
Download the binaries for geckodriver. Ensure they are in your path.

```zsh (.venv) $ curl --location https://github.com/mozilla/geckodriver/releases/download/v0.29.1/geckodriver-v0.29.1-linux64.tar.gz -o geckodriver-v0.29.1-linux64.tar.gz (.venv) $ tar -xvaf geckodriver-v0.29.1-linux64.tar.gz # ensure ./local/bin exists 
# $mkdir .local/bin (.venv) $ mv geckodriver ./local/bin/
```

To check if it is in your path:

```zsh # Standard way but the other ways will work as well depending on Linux version.
(.venv) $ type geckodriver geckodriver is /home/user/.local/bin/geckodriver ```

```zsh (.venv) $ which geckodriver /home/user/.local/bin/geckodriver ```

```zsh (.venv) $ whereis geckodriver geckodriver: /home/user/.local/bin/geckodriver ```

If missing from your path, you will get the following:

```zsh $ type program program not found ```

```zsh $ which program program not found ```

```zsh $ whereis program program:
```

Then add `~/.local/bin` to your path.

```zsh $ export PATH=~/.local/bin:$PATH $
```

## Screenshot 
Shows the folder structure created. Markdown files are under `solves` folder and split out by categories. This is also taken from Magnet Forensics's MVS 2021 CTF.

![folders](images/screenshot.png)
