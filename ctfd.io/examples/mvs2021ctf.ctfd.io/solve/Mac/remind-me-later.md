
# Q#49 - Remind me Later (25)

## Challenges

![challenge](../../qn/49.png)

What time and date in EST did Eli add a notification permission on Safari? Answer in MM/DD/YYYY HH:MM:SS 

### Hints

**None**

### Files

**None**

## Solve

## Answer
