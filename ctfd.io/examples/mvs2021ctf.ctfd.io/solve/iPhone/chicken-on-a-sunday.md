
# Q#68 - Chicken on a Sunday? (25)

## Challenges

![challenge](../../qn/68.png)

Okay, so we know Eli likes Chick-fil-A, what 2 other chain fast food restaurants were visited?  Include both in answer, formatting will not be an issue.   

* Example: DFA-Diner and Magnet Cafe 

### Hints

**None**

### Files

**None**

## Solve

## Answer
