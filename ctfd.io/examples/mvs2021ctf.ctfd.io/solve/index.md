# Challenges

## Mac

- [Q#1 Call me by your name (5)](Mac/call-me-by-your-name.md)
- [Q#44 Whose got your back(up)? (5)](Mac/whose-got-your-backup.md)
- [Q#45 I love it when you call me big sur (5)](Mac/i-love-it-when-you-call-me-big-sur.md)
- [Q#46 Bottoms up, and the devil laughs (10)](Mac/bottoms-up-and-the-devil-laughs.md)
- [Q#47 Stop playing with me (10)](Mac/stop-playing-with-me.md)
- [Q#48 LaxBro (10)](Mac/laxbro.md)
- [Q#49 Remind me Later (25)](Mac/remind-me-later.md)
- [Q#50 Oh Sheet! (25)](Mac/oh-sheet.md)
- [Q#51 Finder's Keepers (25)](Mac/finders-keepers.md)
- [Q#52 it's ra1ning it's pouring (25)](Mac/its-ra1ning-its-pouring.md)
- [Q#53 Secrets Secrets are no fun (50)](Mac/secrets-secrets-are-no-fun.md)
- [Q#54 WallStreetBet You Can't Get This One (50)](Mac/wallstreetbet-you-cant-get-this-one.md)
- [Q#55 Where are my keys!? (50)](Mac/where-are-my-keys.md)
- [Q#56 There are no penguins at the North Pole (50)](Mac/there-are-no-penguins-at-the-north-pole.md)

## iPhone

- [Q#4 Sanik Speed  (5)](iPhone/sanik-speed-.md)
- [Q#5 Sunny Side Up (5)](iPhone/sunny-side-up.md)
- [Q#6 New Watch Who Dis (5)](iPhone/new-watch-who-dis.md)
- [Q#7 Get Zucked! (5)](iPhone/get-zucked.md)
- [Q#16 Breaking Quarntine (5)](iPhone/breaking-quarntine.md)
- [Q#17 Burger Time (5)](iPhone/burger-time.md)
- [Q#8 Getting the Bag (10)](iPhone/getting-the-bag.md)
- [Q#9 Big Spender (10)](iPhone/big-spender.md)
- [Q#10 News Flash (10)](iPhone/news-flash.md)
- [Q#18 Beefstew isn't a Stoganoff Password (10)](iPhone/beefstew-isnt-a-stoganoff-password.md)
- [Q#11 There's No Sign of Intelligent Life Anywhere (15)](iPhone/theres-no-sign-of-intelligent-life-anywhere.md)
- [Q#20 What falls but never hits the ground? (15)](iPhone/what-falls-but-never-hits-the-ground.md)
- [Q#21 What's your number? (15)](iPhone/whats-your-number.md)
- [Q#12 The Epitome of Health (25)](iPhone/the-epitome-of-health.md)
- [Q#13 Give me a signal (25)](iPhone/give-me-a-signal.md)
- [Q#14 You can't beat encryption right? (25)](iPhone/you-cant-beat-encryption-right.md)
- [Q#22 DFIRFit Target (25)](iPhone/dfirfit-target.md)
- [Q#23 Fowl language (25)](iPhone/fowl-language.md)
- [Q#24 Peek-a-boo (25)](iPhone/peekaboo.md)
- [Q#68 Chicken on a Sunday? (25)](iPhone/chicken-on-a-sunday.md)
- [Q#65 Lettuce insert a sandwich pun here (50)](iPhone/lettuce-insert-a-sandwich-pun-here.md)

## Chrome Book

- [Q#28 The folder to store all your data in (5)](Chrome%20Book/the-folder-to-store-all-your-data-in.md)
- [Q#29 Smile for the camera (5)](Chrome%20Book/smile-for-the-camera.md)
- [Q#30 Road Trip! (5)](Chrome%20Book/road-trip.md)
- [Q#42 Promise Me (5)](Chrome%20Book/promise-me.md)
- [Q#31 Key-ty Cat (10)](Chrome%20Book/keyty-cat.md)
- [Q#32 Time to jam out (10)](Chrome%20Book/time-to-jam-out.md)
- [Q#33 Dress for success (10)](Chrome%20Book/dress-for-success.md)
- [Q#43 Autofills, roll out (10)](Chrome%20Book/autofills-roll-out.md)
- [Q#34 It's about the journey not the destination (25)](Chrome%20Book/its-about-the-journey-not-the-destination.md)
- [Q#35 Repeat customer (25)](Chrome%20Book/repeat-customer.md)
- [Q#36 Vroom Vroom (50)](Chrome%20Book/vroom-vroom.md)

## Takeout

- [Q#37 You got mail (5)](Takeout/you-got-mail.md)
- [Q#38 Hungry for directions (10)](Takeout/hungry-for-directions.md)
- [Q#39 Who defines essential?  (10)](Takeout/who-defines-essential-.md)
- [Q#40 I got three subscribers and counting (10)](Takeout/i-got-three-subscribers-and-counting.md)
- [Q#41 Time flies when you're watching YT (10)](Takeout/time-flies-when-youre-watching-yt.md)
- [Q#69 How much? (50)](Takeout/how-much.md)

## Hunt!

- [Q#58 Good Advice (5)](Hunt!/good-advice.md)
- [Q#59 Success (5)](Hunt!/success.md)
- [Q#60 Validation (5)](Hunt!/validation.md)
- [Q#61 Comes Before Time (5)](Hunt!/comes-before-time.md)
- [Q#62 One Off (5)](Hunt!/one-off.md)
- [Q#63 Spam (10)](Hunt!/spam.md)