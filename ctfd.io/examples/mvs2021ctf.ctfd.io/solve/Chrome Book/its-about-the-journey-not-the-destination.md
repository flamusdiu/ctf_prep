
# Q#34 - It's about the journey not the destination (25)

## Challenges

![challenge](../../qn/34.png)

How many miles would the trip have been if Eli took the long way? Answer to the single decimal digit (ex. 9.1).

### Hints

**None**

### Files

**None**

## Solve

## Answer
