# CTF Preparations

## Introduction

The `ctf.py` script configures and setups a directory for each CTF hosted on `ctfd.io` or self-hosted site.

## Usage

```bash
python ctf.py [host] [username] [password]
```

Host is the domain `myctf.ctfd.io`.

| Password is in clear text and will show up in history files. You have been warned! This can be moved out to support prompting.

## Requirements

Requires Python 3.6 or greater.

### Downloads

- Mozilla's Geckodriver used with Seleinum (Linux Only)
  - geckodriver - <https://github.com/mozilla/geckodriver/releases>
- Proxy used with Seleinum for SSL connection
  - Seleinum does not allow modification of headers
  - mitmproxy - <https://mitmproxy.org/> (Linux Only)
- (optional) Selenium - <https://www.selenium.dev/downloads/>
  - If you do not want to use the python library, download the latest from their site.

### Python

- bs4==0.0.1
- mitmproxy==8.1.0
- Pillow==9.1.1
- requests==2.28.1
- selenium==4.2.0

### Installation

#### Virtual Environment

Create a virtual environment and activate it.

```zsh
# Ensure you are in the project folder
$ python3 -m venv .venv
$ source .venv/bin/activate
(.venv) $
```

#### Python Requirements

(.venv) $ python -m pip install -r requirements.txt

#### Download (Linux Only)

Download the binaries for geckodriver and mitmproxy. Ensure they are in your path.

```zsh
(.venv) $ curl --location https://github.com/mozilla/geckodriver/releases/download/v0.29.1/geckodriver-v0.29.1-linux64.tar.gz -o geckodriver-v0.29.1-linux64.tar.gz
(.venv) $ tar -xvaf geckodriver-v0.29.1-linux64.tar.gz
# ensure ./local/bin exists 
# $mkdir .local/bin
(.venv) $ mv geckodriver ./local/bin/
(.venv) $ curl --location https://snapshots.mitmproxy.org/6.0.2/mitmproxy-6.0.2-linux.tar.gz -o mitmproxy-6.0.2-linux.tar.gz
(.venv) $ tar -xvaf mitmproxy-6.0.2-linux.tar.gz
# Moves mitmdump, mitmproxy, and mitmweb
(.venv) $ mv mitm* ./local/bin/
```

To check if it is in your path:

```zsh
# Standard way but the other ways will work as well depending on Linux version.
(.venv) $ type geckodriver
geckodriver is /home/user/.local/bin/geckodriver
```

```zsh
(.venv) $ which geckodriver
/home/user/.local/bin/geckodriver
```

```zsh
(.venv) $ whereis geckodriver
geckodriver: /home/user/.local/bin/geckodriver
```

If missing from your path, you will get the following:

```zsh
$ type program
program not found
```

```zsh
$ which program
program not found
```

```zsh
$ whereis program
program:
```

Then add `~/.local/bin` to your path.

```zsh
$ export PATH=~/.local/bin:$PATH
$
```

## Screenshot

Screenshots from Magnet Forensics's MVS 2021 CTF. The images are inverted with a black border to show up better on white backgrounds.

![output](images/2021-05-15-19-37-47.png)

Shows the folder structure created. Markdown files are under `solves` folder and split out by categories. This is also taken from Magnet Forensics's MVS 2021 CTF.

![folders](images/2021-05-15-19-39-28.png)
