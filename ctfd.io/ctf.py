import json
import os
import platform
import re
import socket
import ssl
import string
import subprocess
import sys
import time
from abc import ABC, abstractmethod
from collections import defaultdict
from dataclasses import dataclass, field
from io import BytesIO
from pathlib import Path
from types import SimpleNamespace as Namespace
from typing import Union

import bs4
import requests
from PIL import Image, ImageOps
from requests.models import Response
from selenium import webdriver
from selenium.common.exceptions import (
    ElementClickInterceptedException,
    NoSuchElementException,
    StaleElementReferenceException,
    TimeoutException,
)
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

ssl._create_default_https_context = ssl._create_unverified_context
requests.packages.urllib3.disable_warnings()


class BrowserProxy:
    def __init__(self):
        self.locale_port()

    def start(self) -> None:
        print(f"\nStarting local proxy on port {self.port}")
        self.proxy = subprocess.Popen(
            [
                "mitmdump",
                "-q",
                "--ssl-insecure",
                "--listen-port",
                str(self.port),
                "--stickycookie",
                ".*",
            ]
        )

        start = time.process_time()
        while True:
            try:
                # check the internal web server to see if working
                resp = requests.get(
                    "http://mitm.it",
                    proxies=BrowserProxy.get_proxy_configuration("requests"),
                    verify=False,
                )
                if resp.status_code == 200:
                    break
            except (ConnectionRefusedError, requests.exceptions.ProxyError) as ex:
                # Prints counter in seconds
                end = time.process_time()
                print(f"waiting ... {round(end - start)}s", end="\r")
                if round(end - start) >= 30:
                    print("Failed to connect to proxy. Exiting...")
                    self.stop()
                    exit(1)
        print("\n")

    def stop(self) -> None:
        print(f"\nTerminating proxy on port {self.port}")
        self.proxy.terminate()

    def locale_port(self) -> None:
        a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        port = 8080

        while True:
            location = ("127.0.0.1", port)
            result_of_check = a_socket.connect_ex(location)
            if result_of_check == 0:
                port += 1
            else:
                BrowserProxy.port = port
                a_socket.close()
                break

    @staticmethod
    def get_proxy_configuration(type="selenium") -> dict:
        if type == "selenium":
            return {
                "proxyType": "manual",
                "httpProxy": f"localhost:{BrowserProxy.port}",
                "sslProxy": f"localhost:{BrowserProxy.port}",
            }
        elif type == "requests":
            return {
                "http": f"http://127.0.0.1:{BrowserProxy.port}",
                "https": f"http://127.0.0.1:{BrowserProxy.port}",
            }
        else:
            return {}


class CTFJson:
    def __set_name__(self, owner, name):
        self.name = str(name)

    def __get__(self, obj, type=None) -> Namespace:
        return obj.__dict__.get(self.name) or None

    def __set__(self, obj, value) -> None:
        if isinstance(value, Response):
            ns = value.json(object_hook=lambda d: Namespace(**d))
            if ns.data:
                obj.__dict__[self.name] = ns.data
            else:
                obj.__dict__[self.name] = ns.game
        elif isinstance(value, list) or isinstance(value, Namespace):
            obj.__dict__[self.name] = value
        else:
            raise TypeError(
                f"Got {type(value)} instead of {type(dict)}!"
                f"Error setting JSON on {str(obj)}"
            )


class FileName:
    def __set_name__(self, owner, name):
        self.name = str(name)

    def __get__(self, obj, type=None) -> str:
        return obj.__dict__.get(self.name) or ""

    def __set__(self, obj, value) -> None:
        if isinstance(value, str):
            delete_dict = {sp_character: "" for sp_character in string.punctuation}

            delete_dict[" "] = "-"
            table = str.maketrans(delete_dict)
            obj.__dict__[self.name] = value.translate(table).lower()
        else:
            raise TypeError(
                f"Got {type(value)} instead of {type(str)}!"
                f"Error setting JSON on {str(obj)}"
            )


class Session:
    def __init__(self) -> None:
        self.logged_in_check_url = ""

    def __call__(
        self,
        uri: str,
        username: str,
        password: str,
        proxy: dict,
        login_path: str = "login",
        logged_in_check_url: str = "",
    ) -> None:
        self.uri = uri
        self.proxy = proxy

        if not self.logged_in_check_url:
            self.logged_in_check_url = logged_in_check_url

        csrf_nonce = self.__csrf_tok()

        self.page_data = {
            "name": username,
            "password": password,
            "nonce": csrf_nonce,
        }
        resp = requests.post(
            f"{uri}/{login_path}",
            data=self.page_data,
            allow_redirects=False,
            proxies=self.proxy,
            verify=False,
        )

        sess_cookie = resp.cookies.get("session")

        assert sess_cookie is not None
        print(f"sess: {sess_cookie}", file=sys.stderr)

    def check_if_logged_in(self):
        resp = requests.get(
            self.logged_in_check_url,
            allow_redirects=False,
            proxies=self.proxy,
            verify=False,
        )

        if resp.status_code == 302:
            print("Logged in failed!")
            return False
        return True

    def __csrf_tok(self) -> str:
        csrf_nonce = None
        resp = requests.get(
            self.uri, allow_redirects=True, proxies=self.proxy, verify=False
        )
        html = resp.content
        soup = bs4.BeautifulSoup(html, "html.parser")
        els = soup.findAll("script", {"src": False})
        for el in els:
            txt = str(el)  # el.text.strip()
            # m = re.search(r"var csrf_nonce = \"([0-9a-f]{64,128})\";?",txt)
            m = re.search(r"'csrfNonce': \"([0-9a-f]{64,128})\",?", txt)
            if m:
                csrf_nonce = m.group(1)
                break
        assert csrf_nonce is not None
        print(f"uri@{self.uri} csrf:{csrf_nonce}", file=sys.stderr)
        return csrf_nonce


sslSession = Session()


@dataclass
class CTFMarkdownAbstract(ABC):
    title: str
    mdSave_dir: Path
    image_save_dir: Path
    mdFile: str = field(init=False, default="")
    file_name: str = field(init=True, default=FileName())
    json: Namespace = field(repr=True, default=CTFJson())

    def save(self):
        print(
            f"--> Markdown saved for {self.title} @ {self.mdSave_dir / self.file_name}.md"
        )
        Path(self.mdSave_dir / f"{self.file_name}.md").write_text(self.mdFile)

    @abstractmethod
    def create(self) -> None:
        NotImplementedError("Need to implement create method!")


@dataclass
class CTFMarkdown(CTFMarkdownAbstract):
    def create(self):
        _id = self.json.id
        title = self.json.name.strip()
        value = self.json.value
        description = self.json.description
        mdFile = f"\n# Q#{_id} - {title} ({value})\n"
        mdFile += f"\n## Challenges\n"
        mdFile += f"\n![challenge](../../qn/{_id}.png)\n"
        mdFile += f"\n{description}\n"
        mdFile += f"\n### Hints\n"
        if self.json.hints:
            mdFile += "\n"
            for hint in self.json.hints:
                if hasattr(hint, "content"):
                    hint_link = Path(f'../../hint/{hint.id}.json')
                    mdFile += f"- Hint [#{hint.id}]({hint_link}) (Cost {hint.cost}): {hint.content}\n"
                else:
                    mdFile += f"- Hint #{hint.id} (Cost {hint.cost}): **Locked**\n"
        else:
            mdFile += "\n**None**\n"
        mdFile += "\n### Files\n"
        if self.json.files:
            mdFile += "\n"
            for file in self.json.files:
                file_link = Path('../files/') / file
                mdFile += f"- [file]({file_link})\n"
        else:
            mdFile += "\n**None**\n"
        mdFile += "\n## Solve\n"
        mdFile += "\n## Answer\n"
        self.mdFile = mdFile


@dataclass
class CTFTableOfContents(CTFMarkdownAbstract):
    def create(self):
        dict_toc = defaultdict(list[Namespace])
        data = self.json
        self.file_name = "index"

        class File:
            name = FileName()

        file_name = File()

        for x in data:
            dict_toc[x.category].append(x)

        mdToc = "# Challenges"

        for category, challenges in dict_toc.items():
            category_link = requests.utils.requote_uri(f"{category}")
            mdCategory = f"\n\n## {category}\n\n"
            challenges_list = []

            for challenge in challenges:
                _id = challenge.id
                value = challenge.value
                name = challenge.name
                file_name.name = challenge.name
                challenges_list.append(
                    f"[Q#{_id} {name} ({value})]({category_link}/{file_name.name}.md)"
                )
            mdCategory += "- " + ("\n- ").join(challenges_list)
            mdToc += mdCategory

        self.mdFile = mdToc


@dataclass
class CTF:
    baseuri_url: str
    page_data: dict = field(init=False)
    baseuri_chall: str = field(init=False)
    baseuri_hint: str = field(init=False)
    baseuri_file: str = field(init=False)
    savedir: Union[str, Path] = field(init=False)
    challenges: list[object] = field(init=False, default_factory=lambda: [])
    json: Namespace = field(init=False, repr=False, default=CTFJson())

    def __post_init__(self) -> None:
        self.baseuri_hint = f"https://{self.baseuri_url}/api/v1/hints"
        self.baseuri_file = f"https://{self.baseuri_url}"
        self.baseuri_chall = f"https://{self.baseuri_url}/api/v1/challenges"
        self.baseuri_me = f"https://{self.baseuri_url}/api/v1/users/me"
        self.proxy = BrowserProxy.get_proxy_configuration(type="requests")

        self.savedir = Path(self.baseuri_url)
        self.savedir.mkdir(parents=True, exist_ok=True)

    def session(self, username: str, password: str, proxy: dict):
        # Start session. Proxy will automatically capture session cookies.
        sslSession(
            uri=f"https://{self.baseuri_url}",
            username=username,
            password=password,
            proxy=proxy,
            logged_in_check_url=self.baseuri_me,
        )

    def get_challenges(self) -> None:
        resp = requests.get(
            self.baseuri_chall, verify=False, allow_redirects=True, proxies=self.proxy
        )
        chatall_json = json.dumps(
            resp.json(), ensure_ascii=False, sort_keys=True, separators=(",", ":")
        )
        outfile = self.savedir / "chall.json"
        outfile.write_text(chatall_json)

        self.json = resp

        assert self.json is not None

    def create_challenges(self) -> None:

        outdir = self.savedir
        data = self.json

        for dir in ["file", "hint", "qn", "solve"]:
            basedir_dir = outdir / dir
            basedir_dir.mkdir(parents=True, exist_ok=True)

        categories = set([x.category for x in data])
        for category in categories:
            basedir_dir = outdir / "solve" / category
            basedir_dir.mkdir(parents=True, exist_ok=True)

        for item in data:
            challenge = CTFChallenge(id=item.id, ctf=self)
            challenge.create()
            self.challenges.append(challenge)

    def create_screenshots(self, driver: webdriver) -> None:
        for item in self.challenges:
            if not item.saved:
                item.screenshot(driver)
                item.saved = True

    def create_toc(self) -> None:
        mdToc = CTFTableOfContents(
            json=self.json,
            title="Challenges",
            mdSave_dir=Path(self.baseuri_url) / "solve",
            image_save_dir=Path(self.baseuri_url) / "images",
            file_name="index",
        )

        mdToc.create()
        mdToc.save()


@dataclass
class CTFChallengeImage:

    challenge: str
    group: str
    element: object = field(init=False)
    location: object = field(init=False)
    size: object = field(init=False)
    save_full_path: Path = field(init=False)
    org_png: Image = field(init=False)
    file_name: str = field(init=True, default_factory=FileName())

    def saved_cropped(self) -> bool:
        im = Image.open(BytesIO(self.org_png))
        im.save(self.save_full_path)

        R, G, B = CTFChallengeImage.get_dominant_color(im)
        im_brightness = sum([R, G, B]) / 3
        if im_brightness < 50:
            im = ImageOps.invert(im.convert("RGB"))

        print(f"--> Saving file {self.save_full_path}...", end=" ")
        im.save(self.save_full_path)

        if self.save_full_path.exists():
            print("successful!")
            return True

        print("failed!")
        return False

    @staticmethod
    def get_dominant_color(pil_img, palette_size=16):
        # Resize image to speed up processing
        img = pil_img.copy()
        img.thumbnail((100, 100))

        # Reduce colors (uses k-means internally)
        paletted = img.convert('P', palette=Image.Palette.ADAPTIVE, colors=palette_size)

        # Find the color that occurs most often
        palette = paletted.getpalette()
        color_counts = sorted(paletted.getcolors(), reverse=True)
        palette_index = color_counts[0][1]
        dominant_color = palette[palette_index * 3 : palette_index * 3 + 3]

        return dominant_color


@dataclass
class CTFChallengeHint:
    id: int
    cost: int
    content: str = ""


@dataclass
class CTFChallengeFile:
    filename: str
    content_type: str
    size: float


@dataclass
class CTFChallenge:
    id: int
    ctf: CTF = field(repr=False)
    description: str = field(init=False, default="")
    name: str = field(init=False, default="")
    category: str = field(init=False, default="")
    image: CTFChallengeImage = field(init=False, default=None)
    mdFile: CTFMarkdown = field(init=False, default="")
    hints: list[CTFChallengeHint] = field(init=False, default_factory=lambda: [])
    files: list[CTFChallengeFile] = field(init=False, default_factory=lambda: [])
    saved: bool = field(init=False, default=False)
    json: Namespace = field(init=False, default=CTFJson())

    def create(self):
        self._save_json()
        self._hints()
        self._files()
        self._markdown()

    def _hints(self) -> None:
        hints = getattr(self.json, "hints", None)
        if hints:
            for hint in hints:
                hint_obj = CTFChallengeHint(
                    id=int(hint.id),
                    cost=int(hint.cost),
                    content=getattr(hint, "content", ""),
                )
                uri = f"{self.ctf.baseuri_hint}/{hint_obj.id}"
                self.hints.append(hint_obj)
                if hint_obj.cost == 0 or hint_obj.content:
                    resp = requests.get(
                        uri, verify=False, allow_redirects=False, proxies=self.ctf.proxy
                    )
                    obj = resp.json()
                    obj_s = json.dumps(
                        obj,
                        ensure_ascii=False,
                        sort_keys=True,
                        separators=(",", ":"),
                    )
                    outfile = Path(self.ctf.savedir / "hint" / f"{hint_obj.id}.json")
                    outfile.write_text(obj_s)
                    print("-->", outfile, file=sys.stderr)
                else:
                    print(
                        f"--> hint: qn#{self.id} idx#{hint_obj.id} cost={hint_obj.cost} @{uri}"
                    )

    def _markdown(self) -> None:
        mdFile_save_dir = Path(self.ctf.savedir / "solve" / self.category)
        image_save_dir = Path(self.ctf.savedir / "qn")
        mdFile = CTFMarkdown(
            title=self.name,
            mdSave_dir=mdFile_save_dir,
            image_save_dir=image_save_dir,
            file_name=self.name,
            json=self.json,
        )
        if not (mdFile_save_dir / f"{mdFile.file_name}.md").exists():
            mdFile.create()
            mdFile.save()
        else:
            print("--> Markdown file already exists for challenge", file=sys.stderr)

    def _files(self) -> None:
        files = getattr(self, "files", None)
        if files:
            for j, file in enumerate(files):
                filename = Path(file).name
                uri = f"{self.ctf.baseuri_file}/{file}"
                resp = requests.get(
                    uri, verify=False, allow_redirects=True, proxies=self.ctf.proxy
                )
                content_type = resp.headers.get("Content-Type")
                content_len = int(resp.headers.get("Content-Length", 0))
                ss = resp.headers.get("Content-Disposition")
                if ss is not None:
                    m = re.search(r"filename=([^;]+)", ss)
                    assert m
                    filename = m.group(1)
                outfile = Path(self.ctf.savedir) / "file" / filename
                outfile.write_bytes(resp.content)
                file_obj = CTFChallengeFile(
                    filename=filename,
                    content_type=content_type,
                    size=content_len,
                )
                self.files[j] = file_obj
                print(
                    f"file: qn#{self.id} idx#{j+1} @{uri} {content_type} {content_len} bytes(s) => {filename}"
                )
                print("->", outfile, file=sys.stderr)

    def screenshot(self, driver: webdriver) -> bool:

        id = self.id
        category = self.category
        name = self.name
        outdir = Path(self.ctf.savedir / "qn")

        print(f"\nGetting {category} - {name}")
        challengeImage = CTFChallengeImage(
            group=category, challenge=name, file_name=name
        )
        challengeImage.save_full_path = Path(outdir) / f"{id}.png"

        if challengeImage.save_full_path.exists():
            print("-> Image exists! Skipping!")
            return

        driver.execute_script("scrollBy(0,-500);")

        WebDriverWait(driver, 30).until(
            EC.invisibility_of_element((By.XPATH, f'//*[@id="challenge-window"]'))
        )

        driver.find_element(
            by=By.XPATH, value=f'//p[text()[contains(., "{name}")]]'
        ).click()

        challengeImage.element = WebDriverWait(driver, 30).until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    "//*[@class='modal-dialog']",
                )
            )
        )

        challengeImage.org_png = driver.get_screenshot_as_png()
        challengeImage.location = challengeImage.element.location
        challengeImage.size = challengeImage.element.size
        challengeImage.saved_cropped()
        self.image = challengeImage
        ActionChains(driver).send_keys(Keys.ESCAPE).perform()
        return True

    def _save_json(self) -> None:
        uri = f"{self.ctf.baseuri_chall}/{self.id}"
        resp = requests.get(
            uri, verify=False, allow_redirects=True, proxies=self.ctf.proxy
        )

        obj = resp.json()
        obj_s = json.dumps(
            obj, ensure_ascii=False, sort_keys=True, separators=(",", ":")
        )

        self.json = resp

        for key, value in obj["data"].items():
            if hasattr(self, key):
                setattr(self, key, value)

        outfile = self.ctf.savedir / "qn" / f"{self.id}.json"
        outfile.write_text(obj_s)
        print("->", outfile, file=sys.stderr)


if __name__ == "__main__":

    baseuri, username, passwd = sys.argv[1:]

    os_version = platform.system()

    if os_version == "Windows":
        webdriver_binary_location = "C:\\Program Files\\Mozilla Firefox\\firefox.exe"
        webdriver_service_log_path = os.path.devnull
    else:
        webdriver_binary_location = "/usr/bin/firefox"
        webdriver_service_log_path = "/dev/null"

    browserproxy = BrowserProxy()
    browserproxy.start()

    ctf = CTF(baseuri)
    ctf.session(
        username=username,
        password=passwd,
        proxy=browserproxy.get_proxy_configuration("requests"),
    )
    if sslSession.check_if_logged_in():
        ctf.get_challenges()
        ctf.create_challenges()

        print("\n Starting grabbing screenshots!")
        while True:
            try:
                options = webdriver.FirefoxOptions()
                options.set_capability(
                    "proxy", browserproxy.get_proxy_configuration(type="selenium")
                )
                options.headless = True
                options.binary_location = webdriver_binary_location
                service = FirefoxService(log_path=webdriver_service_log_path)

                with webdriver.Firefox(
                    service=service,
                    options=options,
                ) as driver:

                    uri = f"https://{baseuri}/challenges"
                    print(f"\nOpening page {uri}...")
                    driver.get(uri)
                    driver.set_window_position(0, 0)
                    driver.set_window_size(1200, 1700)
                    ctf.create_screenshots(driver)
                    print("\nFinished with screenshots!\n")
                    ctf.create_toc()

                break
            except (
                ElementClickInterceptedException,
                TimeoutException,
            ) as ex:
                print(ex.msg)
                print("Error connecting or interacting with page...reopening page!")
    browserproxy.stop()
